<?php
/**
 * Created by PhpStorm.
 * User: irfanhkm
 * Date: 27/02/19
 * Time: 11:11
 */
return [
    'bulan' => array(
                1=>'Januari',2=>'Februari',3=>'Maret',4=>'April',5=>'Mei',6=>'Juni',
                7=>'Juli',8=>'Agustus',9=>'September',10=>'Oktober',11=>'November',12=>'Desember'),
    'tahun' => array(2016,2017,2018,2019,2020),
    'bulan_id' => array(
        ['id' => 1,'bulan' =>'Januari'],
        ['id' => 2,'bulan' =>'Februari'],
        ['id' => 3,'bulan' =>'Maret'],
        ['id' => 4,'bulan' =>'April'],
        ['id' => 5,'bulan' =>'Mei'],
        ['id' => 6,'bulan' =>'Juni'],
        ['id' => 7,'bulan' =>'Juli'],
        ['id' => 8,'bulan' =>'Agustus'],
        ['id' => 9,'bulan' =>'September'],
        ['id' => 10,'bulan' =>'Oktober'],
        ['id' => 11,'bulan' =>'November'],
        ['id' => 12,'bulan' =>'Desember'],
    )
];