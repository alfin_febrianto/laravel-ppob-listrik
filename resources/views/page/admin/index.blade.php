@extends('layout/template')
@section('content')
    <h3>Admin</h3>
    @if (Session::has('type'))
        <div class="alert alert-{!! Session::get('type') !!} alert-dismissable" id="alert">
            <p class="pull-left">{!! Session::get('message') !!}</p>
            <div class="clearfix"></div>
        </div>
    @endif
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default card-view">
                <form action="{{url('admin/add')}}" method="post" id="form-add">
                    <input type="hidden" value="{{ csrf_token() }}" name="_token">
                    <div class="form-group">
                        <label class="control-label mb-10">Nama Admin</label>
                        <input type="text" class="form-control" id="nama_admin"  placeholder="Irfan Hakim" name="nama_admin">
                        <div style="text-transform: capitalize" class="help-block with-errors" id="error"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label mb-10">Username</label>
                        <input type="text" class="form-control" id="username"  placeholder="irfanhkm_" name="username">
                        <div style="text-transform: capitalize" class="help-block with-errors" id="error"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label mb-10">Password</label>
                        <input type="password" class="form-control" id="password"  placeholder="*****" name="password">
                        <div style="text-transform: capitalize" class="help-block with-errors" id="error"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label mb-10">Level</label>
                        <select class="form-control" name="id_level" id="id_level">
                            <option value="" style="display: none;">Pilih Admin</option>
                            @foreach($dataLevel as $level)
                                <option value="{{$level->id_level}}">{{$level->nama_level}}</option>
                            @endforeach
                        </select>
                        <div style="text-transform: capitalize" class="help-block with-errors" id="error"></div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <button type="submit" class="btn cur-p btn-primary">Tambah</button>
                        </div>
                    </div>
                </form>
                <br>
            </div>
        </div>
        <div class="col-md-8">
            <div class="panel panel-default card-view">
                <table class="table table-hover">
                    <tr>
                        <th>No</th><th>Nama Admin</th><th>Username</th><th>Level</th><th>Aksi</th>
                    </tr>
                    <?php $i=1;?>
                    @foreach($dataAll as $data)
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{$data->nama_admin}}</td>
                        <td>{{$data->username}}</td>
                        <td>{{\Request::session()->get('user.id')}}</td>
                        <td>
                            <a style="float: left;margin-right: 1%;" href="#" data-toggle="modal" data-target="#modal"
                               onclick="edit({{$data->id_admin}},'{{$data->nama_admin}}','{{$data->username}}',{{$data->id_level}})" class="btn btn-primary">
                                <span class="fa fa-pencil"></span>
                            </a>
                            @if (\Request::session()->get('user.id')!==$data->id_admin)
                                <form action="{{url('admin/delete')}}" method="post" style="margin: 0;padding: 0;float: left" class="delete">
                                    <input type="hidden" value="{{ csrf_token() }}" name="_token">
                                    <input type="hidden" value="{{ $data->id_admin }}" name="id_admin">
                                    <button  type="submit" class="btn btn-danger btn-delete" >
                                        <span class="fa fa-trash"></span>
                                    </button>
                                </form>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
    <div id="modal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none; padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title">Edit</h5>
                </div>
                <div class="modal-body">
                    <div class="help-block with-errors" id="alert"></div>
                    <form action="{{url('admin/edit')}}" method="POST" id="form-edit">
                        <input type="hidden" value="{{ csrf_token() }}" name="_token">
                        <div class="form-group">
                            <label class="control-label mb-10">Nama Admin</label>
                            <input type="text" class="form-control" id="edit-nama_admin"  placeholder="Irfan Hakim" name="nama_admin">
                            <div style="text-transform: capitalize" class="help-block with-errors" id="error"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label mb-10">Username</label>
                            <input type="text" class="form-control" id="edit-username"  placeholder="irfanhkm_" name="username">
                            <div style="text-transform: capitalize" class="help-block with-errors" id="error"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label mb-10">Level</label>
                            <select class="form-control" name="id_level" id="edit-id_level">
                                <option value="" style="display: none;">Pilih Admin</option>
                                @foreach($dataLevel as $level)
                                    <option value="{{$level->id_level}}">{{$level->nama_level}}</option>
                                @endforeach
                            </select>
                            <input type="hidden" name="id_admin" id="edit-id_admin">
                            <div style="text-transform: capitalize" class="help-block with-errors" id="error"></div>
                        </div>
                        <button type="submit" class="btn cur-p btn-primary">Edit</button>
                        <button type="button" onclick="closeModal()" class="btn btn-default" data-dismiss="modal">Close</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    @if (Session::has('type'))
    <script>
        $("#alert").fadeTo(2000, 500).slideUp(500, function(){
            $("#alert").slideUp(500);
        });
    </script>
    @endif
    <script>
        function edit(id,nama_admin,username,id_level) {
            $('#edit-id_admin').val(id);
            $('#edit-nama_admin').val(nama_admin);
            $('#edit-username').val(username);
            $('#edit-id_level').val(id_level);
        }
    </script>
    <script src="{{asset('dist/js/irfanValidator.js')}}"></script>
@endsection