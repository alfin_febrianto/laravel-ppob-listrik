@extends('layout/template')
@section('content')
    <h3>Penggunaan</h3>
    @if (Session::has('type'))
        <div class="alert alert-{!! Session::get('type') !!} alert-dismissable" id="alert">
            <p class="pull-left">{!! Session::get('message') !!}</p>
            <div class="clearfix"></div>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default card-view">
                <table class="table table-hover">
                    <tr>
                        <th>No</th><th>No Meter</th><th>Nama</th><th>Daya</th>
                    </tr>
                    <?php $i=1;?>
                    @foreach($dataAll as $data)
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{$data->no_meter}}</td>
                        <td>{{$data->nama}}</td>
                        <td>{{$data->tarif->daya}}</td>
                        <td>
                            <a style="float: left;margin-right: 1%;" href="#" data-toggle="modal" data-target="#modal" onclick="add({{$data->id_pelanggan}},'{{$data->nama}}')" class="btn btn-success">
                                <span class="fa fa-plus"></span>
                            </a>
                            <form action="{{url('penggunaan/view')}}" method="post" style="margin: 0;padding: 0;float: left">
                                <input type="hidden" value="{{ csrf_token() }}" name="_token">
                                <input type="hidden" value="{{ $data->id_pelanggan }}" name="id_pelanggan">
                                <button  type="submit" class="btn btn-primary" >
                                    <span class="fa fa-eye"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
    <div id="modal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none; padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title">Edit</h5>
                </div>
                <div class="modal-body">
                    <div class="help-block with-errors" id="alert"></div>
                    <form action="{{url('penggunaan/add')}}" method="POST" id="form-add">
                        <input type="hidden" value="{{ csrf_token() }}" name="_token">
                        <div class="form-group">
                            <label class="control-label mb-10 text-left">Nama</label>
                            <input type="text" class="form-control" id="nama" readonly>
                            <div style="text-transform: capitalize" class="help-block with-errors" id="error"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label mb-10 text-left">Tahun</label>
                            <select class="form-control" id="tahun" name="tahun">
                                <option value="" style="display: none">Pilih Tahun</option>
                                @foreach($tahun as $data)
                                    <option value="<?=$data?>"><?=$data?></option>
                                @endforeach
                            </select>
                            <div style="text-transform: capitalize" class="help-block with-errors" id="error"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label mb-10 text-left">Bulan</label>
                            <select class="form-control" id="bulan" name="bulan">
                                <option value="" style="display: none">Pilih Bulan</option>
                                @foreach ($bulan as $data)
                                    <option value="<?=$data['id']?>"><?=$data['bulan']?></option>
                                @endforeach
                            </select>
                            <div style="text-transform: capitalize" class="help-block with-errors" id="error"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label mb-10 text-left">Meteran Awal - Meteran Akhir</label>
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="text" name="meteran_awal" id="meteran_awal" class="form-control" placeholder="Meteran Awal">
                                    <div style="text-transform: capitalize" class="help-block with-errors" id="error"></div>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name="meteran_akhir" id="meteran_akhir" class="form-control" placeholder="Meteran Akhir">
                                    <div style="text-transform: capitalize" class="help-block with-errors" id="error"></div>
                                </div>
                            </div>
                            <input type="hidden" name="id_pelanggan" id="id_pelanggan">
                            <div class="help-block with-errors" id="false" style="color: red"></div>
                            <button type="submit" class="btn btn-success">Simpan</button>
                            <button type="button" onclick="closeModal()" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    @if (Session::has('type'))
    <script>
        $("#alert").fadeTo(2000, 500).slideUp(500, function(){
            $("#alert").slideUp(500);
        });
    </script>
    @endif
    <script>
        function add(id,nama) {
            $('#id_pelanggan').val(id);
            $('#nama').val(nama);
        }
    </script>
    <script src="{{asset('dist/js/irfanValidator.js')}}"></script>
@endsection