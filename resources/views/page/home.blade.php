@extends('layout/template')
@section('content')
    <h3>Tarif</h3>
    @if (Session::has('type'))
        <div class="alert alert-{!! Session::get('type') !!} alert-dismissable" id="alert">
            <p class="pull-left">{!! Session::get('message') !!}</p>
            <div class="clearfix"></div>
        </div>
    @endif
    <div class="row">
        <div class="col-md-10">
            <div class="panel panel-default card-view">
                <table class="table table-hover">
                    <tr>
                        <th>No</th><th>Deksripsi</th><th>Jumlah Tagihan</th><th>Status</th><th>Aksi</th>
                    </tr>
                    <?php $i=1;?>

                    @foreach($dataAll as $data)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>Bulan {{config("params.bulan.".$data->penggunaan->bulan."")}} / {{$data->penggunaan->tahun}}</td>
                            <td>
                                Rp {{$data->jumlah_tagihan + (5/100 * $data->jumlah_tagihan)}}
                            </td>
                            <td>
                                @if($data->status == 'Belum Bayar')
                                    <span class="label label-danger">Belum Bayar</span>
                                @endif
                                @if($data->status == 'Lunas')
                                    <span class="label label-success">Lunas</span>
                                @endif
                                @if($data->status == 'Ditolak')
                                    <span class="label label-warning">Ditolak</span>
                                @endif
                                @if($data->status == 'Pending')
                                    <span class="label label-primary">Pending</span>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
    {{--<div id="modal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none; padding-right: 17px;">--}}
        {{--<div class="modal-dialog">--}}
            {{--<div class="modal-content">--}}
                {{--<div class="modal-header">--}}
                    {{--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
                    {{--<h5 class="modal-title">Edit</h5>--}}
                {{--</div>--}}
                {{--<div class="modal-body">--}}
                    {{--<div class="help-block with-errors" id="alert"></div>--}}
                    {{--<form action="{{url('tarif/edit')}}" method="POST" id="form-edit">--}}
                        {{--<input type="hidden" value="{{ csrf_token() }}" name="_token">--}}
                        {{--<div class="form-group">--}}
                            {{--<label class="control-label mb-10 text-left">Daya</label>--}}
                            {{--<input type="text" class="form-control" id="edit-daya" placeholder="450"  name="daya">--}}
                            {{--<div class="help-block with-errors" id="error">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--<label class="control-label mb-10" for="exampleInputuname_1">Per/KWH</label>--}}
                            {{--<div class="input-group">--}}
                                {{--<div class="input-group-addon">Rp.</div>--}}
                                {{--<input type="text" class="form-control" id="edit-perkwh"  placeholder="1500" name="perkwh">--}}
                            {{--</div>--}}
                            {{--<div class="help-block with-errors" id="error">--}}
                            {{--</div>--}}
                            {{--<input type="hidden" name="id_tarif" class="form-control" id="edit-id_tarif">--}}
                        {{--</div>--}}
                        {{--<button type="submit" class="btn cur-p btn-primary">Edit</button>--}}
                        {{--<button type="button" onclick="closeModal()" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
@endsection