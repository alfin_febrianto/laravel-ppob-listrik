<?php namespace App;

use App\Http\Requests\Request;
use Illuminate\Database\Eloquent\Model;

class Tarif extends Model {

    protected $table = 'tarif';

    protected $fillable = ['daya', 'perkwh'];

    protected $primaryKey = 'id_tarif';

    protected $guarded = ['id_tarif'];

    public $timestamps = false;



    public function rules($id_tarif)
    {
        return [
            'daya' => 'required|numeric|unique:tarif,daya,'.$id_tarif.',id_tarif',
            'perkwh' => 'required|numeric'
        ];
    }

    public function pelanggan(){
        return $this->hasOne('App\User');
    }

}
