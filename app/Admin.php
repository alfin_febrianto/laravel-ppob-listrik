<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model {

    protected $table = 'admin';
    protected $fillable = ['id_level','username','password','nama_admin'];
    protected $primaryKey = 'id_admin';
    protected $guarded = ['id_admin'];
    public $timestamps = false;

    public function rules($id_admin)
    {
        return [
            'nama_admin' => 'required|min:2',
            'username' => 'required|min:4|unique:admin,username,'.$id_admin.',id_admin',
            'password' => 'required_if:id_admin,==,""|min:4',
            'id_level' => 'required'
        ];
    }
    public function attributes(){
        return [
            'id_level' => 'Level'
        ];
    }

	public function login($req)
	{
		return Admin::where('username',$req->username)->where('password',md5($req->password))->first();
	}

    public function level(){
        return $this->belongsTo('App\Level','id_level');
    }

}
