<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model {

    public $timestamps = false;
    protected $table = 'pembayaran';
        protected $fillable = ['id_tagihan','tanggal_bayar','biaya_admin','total_bayar','bukti_pembayaran'];
    protected $primaryKey = 'id_pembayaran';
    protected $guarded = ['id_pembayaran'];

    public function rules()
    {
        return [
            'id_tagihan' => 'required',
            'tanggal_bayar' => 'required|date',
            'bukti_pembayaran' => 'required'
        ];
    }

    public function tagihan(){
        return $this->belongsTo('App\Tagihan','id_tagihan');
    }

}