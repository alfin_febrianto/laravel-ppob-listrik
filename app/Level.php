<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Level extends Model {

    protected $table = 'level';

    protected $fillable = ['nama_level'];

    protected $primaryKey = 'id_level';

    protected $guarded = ['id_level'];

    public $timestamps = false;



    public function rules($id_level)
    {
        return [
            'nama_level' => 'required|min:4|unique:level,nama_level,'.$id_level.',id_level',
        ];
    }

    public function admin(){
        return $this->hasOne('App\Admin');
    }

}
