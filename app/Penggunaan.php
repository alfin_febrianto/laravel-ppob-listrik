<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Penggunaan extends Model {

    public $timestamps = false;
    protected $table = 'penggunaan';
    protected $fillable = ['bulan','tahun','meteran_awal','meteran_akhir','id_pelanggan'];
    protected $primaryKey = 'id_penggunaan';
    protected $guarded = ['id_penggunaan'];

    public function rules()
    {
        return [
            'bulan' => 'required',
            'tahun' => 'required',
            'meteran_awal' => 'required|numeric',
            'meteran_akhir' => 'required|numeric',
            'id_pelanggan' => 'required'
        ];
    }

    public function pelanggan(){
        return $this->belongsTo('App\Pelanggan','id_pelanggan');
    }

    public function tagihan(){
        return $this->hasMany('App\Tagihan','id_penggunaan');
    }
}