<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');
Route::get('home', 'HomeController@index');

Route::get('login',function(){
	if(Session::get('login')){
		return redirect('home');
	}else{
		return view('login.login',['action'=>'login']);
	}
});

Route::get('/login/admin',function(){
	if(Session::get('login')){
		return redirect('/home');
	}else{
		return view('login.login',['action'=>'login/admin']);
	}
});

Route::get('/logout', function(){
	Session::flush();
	return redirect('login');
});

Route::post('login','AuthController@login');
Route::post('login/admin','AuthController@loginAdmin');

//Tarif
Route::get('tarif', 'TarifController@index');
Route::post('tarif/add','TarifController@add');
Route::post('tarif/edit','TarifController@edit');
Route::post('tarif/delete','TarifController@delete');

//Pelanggan
Route::get('pelanggan', 'PelangganController@index');
Route::post('pelanggan/add','PelangganController@add');
Route::post('pelanggan/edit','PelangganController@edit');
Route::post('pelanggan/delete','PelangganController@delete');

//Admin
Route::get('admin', 'AdminController@index');
Route::post('admin/add','AdminController@add');
Route::post('admin/edit','AdminController@edit');
Route::post('admin/delete','AdminController@delete');

//Penggunaan
Route::get('penggunaan', 'PenggunaanController@index');
Route::post('penggunaan/add','PenggunaanController@add');
Route::post('penggunaan/view','PenggunaanController@view');
Route::post('penggunaan/delete','PenggunaanController@delete');
