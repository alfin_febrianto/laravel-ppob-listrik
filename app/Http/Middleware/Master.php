<?php namespace App\Http\Middleware;

use Closure;
use Session;
use Alert;

class Master{
    public function handle($request, Closure $next)
    {
        if(Session::get('user.id_level')==1){
            return $next($request);
        }else{
            Alert::error('Anda tidak boleh mengakses ini', 'Error');
            return redirect('home');
        }
    }
}
