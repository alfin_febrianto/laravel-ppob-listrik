<?php namespace App\Http\Controllers;
use Session;
use Illuminate\Http\Request;
use App\Pelanggan;
use App\Penggunaan;
use App\Tagihan;
use Exception;
use Validator;
use Alert;

class PenggunaanController extends Controller {

    public function __construct()
    {
        $this->middleware('login');
        $this->middleware('admin');
        $this->middleware('master');
        if (Session::get('admin')){
            $this->navbar = true;
        }
        $this->getRedirectUrl = 'penggunaan';
    }

    public function index()
    {
        $data['navbar'] = $this->navbar;
        $data['dataAll'] = Pelanggan::all();
        $data['tahun'] = config('params.tahun');
        $data['bulan'] = config('params.bulan_id');
        return view('page/penggunaan/index')->with($data);
    }

    public function add(Request $req){
        $penggunaan = new Penggunaan();
        $validator = Validator::make($req->all(),$penggunaan->rules());
        if ($validator->fails()) {
            $data['error'] = true;
            $data['error_msg']= $validator->errors();
            return response()->json($data);
        }else{
            $count = Penggunaan::where('id_pelanggan','=',$req->id_pelanggan)->where('bulan','=',$req->bulan)->where('tahun','=',$req->tahun)->count();
            if ($count){
                $data['false'] = true;
                $data['false_msg']= 'Penggunaan Sudah Ada';
            }else if ($req->meteran_awal>$req->meteran_akhir){
                $data['false'] = true;
                $data['false_msg']= 'Input Meteran Salah';
            }else{
                $penggunaan->id_pelanggan = $req->id_pelanggan;
                $penggunaan->bulan = $req->bulan;
                $penggunaan->tahun = $req->tahun;
                $penggunaan->meteran_awal = $req->meteran_awal;
                $penggunaan->meteran_akhir = $req->meteran_akhir;
                $penggunaan->save();
                $tagihan = new Tagihan();
                $pelanggan = Pelanggan::find($req->id_pelanggan);
                $tagihan->id_penggunaan = $penggunaan->id_penggunaan;
                $tagihan->jumlah_meter = $req->meteran_akhir - $req->meteran_awal;
                $tagihan->jumlah_tagihan = ($req->meteran_akhir - $req->meteran_awal) * $pelanggan->tarif->perkwh;
                $tagihan->status = 'Belum Bayar';
                $tagihan->save();
                $data['success'] = true;
                $data['redirect'] = $this->getRedirectUrl;
            }
            return response()->json($data);
        }
    }

    public function view (Request $req){
        $data['navbar'] = $this->navbar;
        $data['backUrl'] = $this->getRedirectUrl;
        $data['dataAll'] =  Tagihan::with('penggunaan','penggunaan.pelanggan','penggunaan.pelanggan.tarif')->whereHas('penggunaan',
            function($users) use($req) {
                $users->where('id_pelanggan','=',$req->id_pelanggan);
            })->get();
        return view('page/penggunaan/view')->with($data);
    }

    public function delete (Request $req){
        $modelTagihan = Tagihan::find($req->id_tagihan);
        $modelPenggunaan = Penggunaan::find($req->id_penggunaan);
        $status = $req->status;
        if ($modelTagihan && $modelPenggunaan && $status == 'Belum Bayar' && $req->session()->get('user.id_level')==1){
            try{
                $modelTagihan->delete();
                $modelPenggunaan->delete();
                Alert::success('Data berhasil dihapus', 'Success')->autoclose(1000);
            }catch(Exception $e){
                Alert::error('Data gagal Dihapus', 'Error');
                return redirect($this->getRedirectUrl);
            }
            return redirect($this->getRedirectUrl);
        }else{
            Alert::error('Data gagal Dihapus', 'Error');
            return redirect($this->getRedirectUrl);
        }
    }
}