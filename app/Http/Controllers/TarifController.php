<?php namespace App\Http\Controllers;
use Session;
use Illuminate\Http\Request;
use App\Tarif;
use Validator;
use Exception;
use Alert;

class TarifController extends Controller {

    public function __construct()
    {
        $this->middleware('login');
        $this->middleware('admin');
        $this->middleware('master');
        if (Session::get('admin')){
            $this->navbar = true;
        }
    }

    public function index()
    {
        $data['navbar'] = $this->navbar;
        $data['dataAll'] = Tarif::all();
        return view('page/tarif/index')->with($data);
    }

    public function add(Request $req){
        $model = new Tarif();
        $validator = Validator::make($req->all(),$model->rules($req->id_tarif));
        if ($validator->fails()) {
            $data['error'] = true;
            $data['error_msg']= $validator->errors();
            return response()->json($data);
        }else{
            $model->daya = $req->daya;
            $model->perkwh = $req->perkwh;
            $model->save();
            $data['success'] = true;
            $data['redirect'] = 'tarif';
            return response()->json($data);
        }
    }

    public function edit(Request $req){
        $model = Tarif::find($req->id_tarif);
        $validator = Validator::make($req->all(),$model->rules($req->id_tarif));
        if ($validator->fails()) {
            $data['error'] = true;
            $data['error_msg']= $validator->errors();
            return response()->json($data);
        }else{
            $model->daya = $req->daya;
            $model->perkwh = $req->perkwh;
            $model->save();
            $data['success'] = true;
            $data['redirect']= '/tarif';
            return response()->json($data);
        }
    }

    public function delete(Request $req){
        $model = Tarif::find($req->id_tarif);
        if ($model){
            try{
                $model->delete();
                Alert::success('Data berhasil dihapus', 'Success')->autoclose(1000);
            }catch(Exception $e){
                Alert::error('Data gagal Dihapus', 'Error');
                return redirect('tarif');
            }
            return redirect()->action('TarifController@index');
        }else{
            Alert::error('Data gagal Dihapus', 'Error');
            return redirect('tarif');
        }
    }
}