<?php namespace App\Http\Controllers;
use App\Penggunaan;
use Illuminate\Http\Request;
use Session;
use App\Tagihan;
use App\Pelanggan;

class HomeController extends Controller {
	
	public function __construct()
	{
		$this->middleware('login');
        if (Session::get('admin')){
            $this->navbar = true;
            $this->layout = 'page/dashboard';
        }else{
            $this->layout = 'page/home';
            $this->navbar = false;
        }
	}

	public function index(Request $req)
	{
	    if ($this->layout == 'page/home'){
            $data['dataAll'] =  Tagihan::with('penggunaan','penggunaan.pelanggan')->whereHas('penggunaan',
                function($users) use ($req) {
                $users->where('id_pelanggan','=',$req->session()->get('user.id'));
            })->get();;
        }
	    $data['navbar'] = $this->navbar;
		return view($this->layout)->with($data);
	}

}
