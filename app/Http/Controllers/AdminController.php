<?php namespace App\Http\Controllers;
use Session;
use Illuminate\Http\Request;
use App\Admin;
use App\Level;
use Validator;
use Exception;
use Alert;

class AdminController extends Controller {

    public function __construct()
    {
        $this->middleware('login');
        $this->middleware('admin');
        $this->middleware('master');
        if (Session::get('admin')){
            $this->navbar = true;
        }
    }

    public function index()
    {
        $data['navbar'] = $this->navbar;
        $data['dataAll'] = Admin::all();
        $data['dataLevel'] = Level::all();
        return view('page/admin/index')->with($data);
    }

    public function add(Request $req){
        $model = new Admin();
        $validator = Validator::make($req->all(),$model->rules($req->id_admin));
        $validator->setAttributeNames($model->attributes());
        if ($validator->fails()) {
            $data['error'] = true;
            $data['error_msg']= $validator->errors();
            return response()->json($data);
        }else{
            $model->nama_admin= $req->nama_admin;
            $model->username= $req->username;
            $model->password= md5($req->password);
            $model->id_level= $req->id_level;
            $model->save();
            $data['success'] = true;
            $data['redirect'] = 'admin';
            return response()->json($data);
        }
    }

    public function edit(Request $req){
        $model = Admin::find($req->id_admin);
        $validator = Validator::make($req->all(),$model->rules($req->id_admin));
        if ($validator->fails()) {
            $data['error'] = true;
            $data['error_msg']= $validator->errors();
            return response()->json($data);
        }else{
            $model->nama_admin= $req->nama_admin;
            $model->username= $req->username;
            $model->id_level= $req->id_level;
            $model->save();
            $data['success'] = true;
            $data['redirect']= 'admin';
            return response()->json($data);
        }
    }

    public function delete(Request $req){
        if ($req->session()->get('user.id')!==$req->id_admin){
            $model = Admin::find($req->id_admin);
            if ($model){
                try{
                    $model->delete();
                    Alert::success('Data berhasil dihapus', 'Success')->autoclose(1000);
                }catch(Exception $e){
                    Alert::error('Data gagal Dihapus', 'Error');
                    return redirect('admin');
                }
                return redirect()->action('AdminController@index');
            }else{
                Alert::error('Data gagal Dihapus', 'Error');
                return redirect('admin');
            }
        }else{
            Alert::error('Noob', 'Error');
            return redirect('admin');
        }
    }
}