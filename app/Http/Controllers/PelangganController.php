<?php namespace App\Http\Controllers;
use Session;
use Illuminate\Http\Request;
use App\Pelanggan;
use App\Tarif;
use Validator;
use Exception;
use Alert;

class PelangganController extends Controller {

    public function __construct()
    {
        $this->middleware('login');
        $this->middleware('admin');
        $this->middleware('master');
        if (Session::get('admin')){
            $this->navbar = true;
        }
    }

    public function index()
    {
        $data['navbar'] = $this->navbar;
        $data['dataAll'] = Pelanggan::all();
        $data['dataTarif'] = Tarif::all();
        return view('page/pelanggan/index')->with($data);
    }

    public function add(Request $req){
        $model = new Pelanggan();
        $validator = Validator::make($req->all(),$model->rules($req->id_pelanggan));
        $validator->setAttributeNames($model->attributes());
        if ($validator->fails()) {
            $data['error'] = true;
            $data['error_msg']= $validator->errors();
            return response()->json($data);
        }else{
            $model->no_meter= $req->no_meter;
            $model->nama= $req->nama;
            $model->username= $req->username;
            $model->password= md5($req->password);
            $model->alamat= $req->alamat;
            $model->id_tarif= $req->id_tarif;
            $model->save();
            $data['success'] = true;
            $data['redirect'] = 'pelanggan';
            return response()->json($data);
        }
    }

    public function edit(Request $req){
        $model = Pelanggan::find($req->id_pelanggan);
        $validator = Validator::make($req->all(),$model->rules($req->id_pelanggan));
        if ($validator->fails()) {
            $data['error'] = true;
            $data['error_msg']= $validator->errors();
            return response()->json($data);
        }else{
            $model->no_meter= $req->no_meter;
            $model->nama= $req->nama;
            $model->username= $req->username;
            $model->alamat= $req->alamat;
            $model->id_tarif= $req->id_tarif;
            $model->save();
            $data['success'] = true;
            $data['redirect']= 'pelanggan';
            return response()->json($data);
        }
    }

    public function delete(Request $req){
        $model = Pelanggan::find($req->id_pelanggan);
        if ($model){
            try{
                $model->delete();
                Alert::success('Data berhasil dihapus', 'Success')->autoclose(1000);
            }catch(Exception $e){
                Alert::error('Data gagal Dihapus', 'Error');
                return redirect('pelanggan');
            }
            return redirect()->action('PelangganController@index');
        }else{
            Alert::error('Data gagal Dihapus', 'Error');
            return redirect('pelanggan');
        }
    }
}